import React from 'react';
import ReactDOM from 'react-dom';

const root = ReactDOM.createRoot(document.getElementById('react-root'));
root.render(
  <h2>Hello world! This is test</h2>
);